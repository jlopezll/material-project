import { User } from './../models/User';
import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';


const httpOptions = {
    headers: new HttpHeaders({"Content-Type": "application/json"})
};
const apiUrl: string = "http://localhost:3000/user";

@Injectable()
export class UserService {

    constructor(private http: HttpClient) { }

    getUsers(): Observable<User[]> {
        const http$: Observable<User[]> = this.http.get<User[]>(apiUrl);
        http$.subscribe(
            res => console.log("HTTP response", res),
            err => console.log("HTTP Error", err),
            () => console.log("HTTP request completed.")
        );
        return http$;
    }

    addUser(user: User): Observable<User> {
        const http$: Observable<User> = this.http.post<User>(apiUrl, user, httpOptions);
        http$.subscribe(
            res => console.log("HTTP response", res),
            err => console.log("HTTP Error", err),
            () => console.log("HTTP request completed.")
        );
        return http$;
    }
}
